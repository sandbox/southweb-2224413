This module uses NZ Post's Ratefinder API to provide uc_quote lookups

Usage
After enabling the module, configure here:

admin/store/settings/quotes (enable quote method here)
admin/store/settings/quotes/methods/ratefinder 
(set the API Key and configure here)

Untested results for Domestic rates
